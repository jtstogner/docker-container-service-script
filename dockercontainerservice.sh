#!/bin/bash
  # ./etc/init.d/functions

CONTAINER="type_your_container_name_or_id_here";

  start() {
    STATUS=$(getStatus);
      if [ "$STATUS" == "false" ]; then
        echo "Starting docker container: $CONTAINER ";
        RESULT=$(docker start $CONTAINER);
        if [ "$RESULT" == "$CONTAINER" ]; then
          echo "Container $CONTAINER successfully started";
        else
          echo "Container $CONTAINER failed to start please check the logs";
          echo "Example:"
          echo "docker logs $CONTAINER";
        fi
      else
        echo "Docker container $CONTAINER is already running.";
        echo "Try stopping or restarting.";
        echo "Example:";
        echo "  $0 stop";
        echo "OR"
        echo "  $0 restart";
      fi
  }

  stop() {
    STATUS=$(getStatus);
      if [ "$STATUS" == "true" ]; then
        echo "Stopping docker container: $CONTAINER";
        RESULT=$(docker stop $CONTAINER);
        if [ "$RESULT" == "$CONTAINER" ]; then
          echo "Container $CONTAINER successfully stopped.";
        else
          echo "Container $CONTAINER failed to start please check the logs";
          echo "Example:"
          echo "docker logs $CONTAINER";
        fi
      else
        echo "Docker container $CONTAINER is not running.";
        echo "Try starting the service.";
        echo "Example:";
        echo "  $0 start";
      fi
  }

  status(){
    STATUS=$(getStatus);
    if [ "$STATUS" == "true" ]; then
      echo "Docker container $CONTAINER is running.";
    else
      echo "Docker container $CONTAINER is not running.";
    fi
  }

  restart(){
    STATUS=$(getStatus);
    if [ "$STATUS" == "true" ]; then
      stop;
      start;
    else
      start;
    fi
  }

  getStatus(){
    NEWSTATUS=$(docker  inspect --format="{{ .State.Running }}" $CONTAINER 2> /dev/null);
    echo "$NEWSTATUS";
  }


  case "$1" in
      start)
         start
         ;;
      stop)
         stop
         ;;
      restart)
         restart
         ;;
      status)
         status
         ;;
      *)
         echo "Usage: $0 {start|stop|status|restart}"
  esac

  exit 0
